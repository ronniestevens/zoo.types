from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from Products.Five.browser import BrowserView


class BannerView(BrowserView):
    """default view for banner 
    """

    index = ViewPageTemplateFile("templates/banner_view.pt")

    def __init__(self, context, request):
        self.context = context
        self.request = request


    def render(self):
        return self.index()

    def __call__(self):
        return self.render()

        

    
