from plone.supermodel import model
from zope import schema
from plone.app.textfield import RichText
from plone.namedfile.field import NamedBlobImage
from plone.directives import form
from z3c.relationfield.schema import RelationChoice
from plone.formwidget.contenttree import ObjPathSourceBinder
from plone.formwidget.contenttree import ContentTreeFieldWidget

from zoo.types import _


class IBanner(model.Schema):
    """Dexterity-schema for banner 
    """

    body = RichText(
        title=_(u"body"),
        description=_(u"Description"),
        required=False,)

    image = NamedBlobImage(
        title=_(u"Image"),
        description=_(u"940x300 pixels"),
        required=False,)
    
    form.widget (
        internalLink=ContentTreeFieldWidget)

    internalLink = RelationChoice(
        title=_(u"Internal Link"),
        source=ObjPathSourceBinder(),
        required=False,)
  
    internalLinkTitle = schema.TextLine(
        title=_(u"Internal link title"),
        description=_(u"Visible title of link"),
        required=False,)

    video = schema.TextLine(
        title=_(u"Video code"),
        description=_(u"Only fill in Vimeo videoid"),
        required=False,)

